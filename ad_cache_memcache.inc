<?php

/**
 * @file
 * Memcache include.
 *
 * Copyright (c) 2005-2009.
 *   Jeremy Andrews <jeremy@tag1consulting.com>.
 */

/**
 * Initialization function.
 */
function ad_cache_memcache_open() {
  _debug_echo('Ad memcache: open');
  return ad_memcache_init();
}

/**
 * Return hook definition.
 */
function ad_cache_memcache_hook($hook) {
  static $cache = NULL;
  _debug_echo("Ad memcache: hook($hook)");

  if (empty($cache)) {
    _debug_echo('Ad memcache: retrieving hook info from cache.');
    $cache = ad_memcache_get('ad-cache-hooks');
  }
  if (isset($cache["hook_$hook"]) && is_array($cache["hook_$hook"])) {
    return $cache["hook_$hook"];
  }
  else {
    _debug_echo("File cache: hook '$hook' not found.");
  }
  return array();
}

/**
 * Return hook definition.
 */
function ad_cache_memcache_get_cache($data = NULL) {
  static $cache = NULL;

  if (!isset($cache)) {
    $cache = ad_memcache_get('ad-cache-hooks');
    if (isset($cache['external-hooks'])) {
      $cache = $cache['external-hooks'];
    }
    else {
      $cache = array();
    }
  }

  if (isset($cache[$data])) {
    return $cache[$data];
  }
  else {
    return array();
  }
}

function ad_cache_memcache_id($type, $id, $hostid) {
  _debug_echo("Ad memcache: get id type($type) id($id) hostid($hostid)");
  switch ($type) {
    case 'host':
      // TODO:
      break;

    case 'nids':
      return explode(',', $id);

    case 'tids':
      $ids = ad_memcache_get("ad-taxonomy-cache-$id");
      if (!$ids || empty($ids)) {
        $taxonomy = ad_memcache_get('ad-taxonomy');
        $cache = array();
        $ids = explode(',', $id);
        foreach ($ids as $tid) {
          if (is_array($taxonomy[$tid])) {
            $cache += $taxonomy[$tid];
          }
        }
        // Rebuild keys from 0, cache for quick re-use on next ad display.
        $ids = array_values($cache);
        ad_memcache_set("ad-taxonomy-cache-$id", $ids);
      }
      return $ids;

    case 'default':
      $taxonomy = ad_memcache_get('ad-taxonomy');
      return $taxonomy[0];

    default:
      _debug_echo("Ad memcache: unknown id tpye '$type'.");
      break;
  }
}

/**
 * Filter out advertisements that don't exist, or have already been displayed.
 */
function ad_cache_memcache_validate($ads, $displayed, $hostid) {
  $valid = array();
  if (is_array($ads)) {
    $valid_ads = ad_memcache_get('ad-ads');
    foreach ($ads as $aid) {
      // Only include aids that are in our cache, others are not valid in our
      // context.  Also, don't display the same ad twice.
      if (isset($valid_ads[$aid]) && !in_array($aid, $displayed)) {
        $valid[] = $aid;
      }
    }
  }
  if (adserve_variable('debug')) {
    $count = sizeof($valid);
    _debug_echo("Ad memcache: found $count valid advertisements.");
  }
  return $valid;
}

/**
 * Display the actual advertisement.
 */
function ad_cache_memcache_display_ad($aid) {
  // TODO: Validate hostid.
  $hostid = adserve_variable('hostid') ? adserve_variable('hostid') : 'none';
/*
  if ($hostid != 'none' && !isset($cache['hostid'][$hostid])) {
    _debug_echo("File cache: invalid hostid: '$hostid'.");
    $output = 'You do not have permission to display ads.';
  }
  else {
*/
  $ad = ad_memcache_get_ad($aid);
  return $ad->display;
}

function ad_memcache_get_ad($aid) {
  static $load = FALSE;

  $ad = ad_memcache_get("ad-aid-$aid");

  if (!$load && !is_object($ad)) {
    $load = TRUE;
    adserve_bootstrap();
    $ad_memcache_build = variable_get('ad_memcache_build', '');
    if ((time() - $ad_memcache_build) >= 60) {
      ad_memcache_build();
      $ad = ad_memcache_get("ad-aid-$aid");
    }
  }

  return $ad;
}

/**
 * Increment impressions counter in memcache.
 */
function ad_cache_memcache_increment($action, $aid) {
  static $timestamp = NULL;

  if (!isset($timestamp)) {
    $timestamp = date('YmdH');
  }
  $group = adserve_variable('group');
  $hostid = adserve_variable('hostid') ? adserve_variable('hostid') : 'none';

  $extra = adserve_invoke_hook('increment_extra', 'merge', $action, $aid);
  if (is_array($extra) && !empty($extra)) {
    $extra = implode('|,|', $extra);
  }
  else if (empty($extra)) {
    $extra = '';
  }
  adserve_variable('extra', $extra);

  _debug_echo("Memcache: increment action($action) aid($aid) group($group) hostid($hostid) extra($extra).");

  $counters = ad_memcache_get("ad-counters-$aid");
  $update = TRUE;
  if (!is_array($counters) || !isset($counters["$action:$group:$hostid:$extra:$timestamp"])) {
    _debug_echo("Memcache: adding map: action($action) aid($aid) group($group) hostid($hostid) extra($extra) timestamp($timestamp)");
    ad_memcache_increment_map($action, $aid, $group, $hostid, $extra, $timestamp);
  }

  $rc = ad_memcache_increment("ad-$action-$aid-$group-$hostid-$extra-$timestamp");
  _debug_echo("Memcache: incrementing ad-$action-$aid-$group-$hostid-$extra-$timestamp ($rc)");
}

/**
 * The maximum time any process can hold a given lock, in seconds.
 */
define('AD_MEMCACHE_LOCK_LIMIT', 2);

/**
 * Store a value in memcache.
 */
function ad_memcache_set($key, $value, $timeout = 86400) {
  $memcache = ad_memcache_init();

  return $memcache->set($key, $value, MEMCACHE_COMPRESSED, $timeout);
}

/**
 * Store a value in memcache.
 */
function ad_memcache_add($key, $value, $timeout = 86400) {
  $memcache = ad_memcache_init();

  return $memcache->add($key, $value, MEMCACHE_COMPRESSED, $timeout);
}

/**
 * Get a value from memcache.
 */
function ad_memcache_get($key) {
  $memcache = ad_memcache_init();

  return $memcache->get($key);
}

/**
 * Delete a value from memcache.
 */
function ad_memcache_delete($key) {
  $memcache = ad_memcache_init();

  return $memcache->delete($key);
}

/**
 * Get a lock in memcache.
 */
function ad_memcache_lock($key, $wait = TRUE) {
  $loop = 0;
  $lock = FALSE;
  while ($lock == FALSE) {
    $lock = ad_memcache_add("LOCK-$key-LOCK", TRUE, AD_MEMCACHE_LOCK_LIMIT);
    if (!$lock && $wait) {
      if ($loop++ > 50) {
        // Hard limit of 5 seconds, after which we fail to grab a lock.
        return FALSE;
      }
      // Wait 1/10th of a second and try again.
      usleep(100000);
    }
    else if (!$lock && !$wait) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Release a lock in memcache.
 */
function ad_memcache_unlock($key) {
  ad_memcache_delete("LOCK-$key-LOCK");
}

/**
 * Increment a numerical value in memcache.
 */
function ad_memcache_increment($key, $value = 1) {
  $memcache = ad_memcache_init();

  $rc = $memcache->increment($key, $value);
  if ($rc === FALSE) {
    // We tried incrementing a counter that hasn't yet been initialized.
    $rc = $memcache->set($key, $value);
    if ($rc === FALSE) {
      // Another process already initialized the counter, increment it.
      $rc = $memcache->increment($key);
    }
  }
  return $rc;
}

/**
 * Decrement a numerical value in memcache.
 */
function ad_memcache_decrement($key, $value = 1) {
  $memcache = ad_memcache_init();

  $rc = $memcache->decrement($key, $value);
  if ($rc === FALSE) {
    // We tried incrementing a counter that hasn't yet been initialized.
    $rc = $memcache->set($key, $value);
    if ($rc === FALSE) {
      // Another process already initialized the counter, increment it.
      $rc = $memcache->decrement($key);
    }
  }
  return $rc;
}

/**
 * Update mapping which allows us to quickly find stats in memcache when
 * feeding them into the database.
 */
function ad_memcache_increment_map($action, $aid, $group, $hostid, $extra, $timestamp) {
  $key = "ad-counters-$aid";
  if (ad_memcache_lock($key)) {
    $counters = ad_memcache_get($key);
    if (!is_array($counters) || empty($counters) || !isset($counters["$action:$group:$hostid:$extra:$timestamp"])) {
      $counters["$action:$group:$hostid:$extra:$timestamp"] = "$action:$group:$hostid:$extra:$timestamp";
      ad_memcache_set($key, $counters);
    }
    ad_memcache_unlock($key);
  }
}

/**
 * Decrement a numerical value in memcache.
 * TODO: Use the same configuration style as Drupal's memcache module,
 * supporting multiple memcache servers, etc.
 */
function ad_memcache_init() {
  static $memcache = NULL;

  if (!$memcache) {
    _debug_echo("Ad memcache: init, adding server host(localhost) port(11211)");
    $memcache = new Memcache;
    $memcache->addServer('localhost', 11211);
  }
  return $memcache;
}

/**
 * Allow external ad selection logic.
 */
function ad_memcache_adserve_select($ads, $invalid) {
  $cache = array();
  if ($select_func = ad_memcache_hook($cache, 'include_file_select', 'include_func_select')) {
    _debug_echo("Memcache: adserve_select: invoking '$select_func()'");
    if (function_exists($select_func)) {
      if (is_array($cache) && !empty($cache)) {
        return $select_func($ads, $invalid, $cache);
      }
      else {
        _debug_echo("Memcache: unexpected error: cache empty.");
      }
    }
    else {
      _debug_echo("Memcache: adserve_select: '$include_func_select()' not found");
    }
  }
  else {
    _debug_echo("Memcache: adserve_select: no select function defined");
  }
}

/**
 * Allow external exit text.
 */
function ad_memcache_adserve_exit_text() {
  $cache = array();
  if ($exit_text_func = ad_memcache_hook($cache, 'include_file_exit_text', 'include_func_exit_text')) {
    _debug_echo("Memcache: adserve_exit_text: invoking '$exit_text_func()'");
    if (function_exists($exit_text_func)) {
      return $exit_text_func();
    }
    else {
      _debug_echo("Memcache: adserve_exit_text: '$exit_text_func()' not found");
    }
  }
  else {
    _debug_echo("Memcache: adserve_exit_text: no exit_text function defined");
  }
}

