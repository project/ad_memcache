---------
Overview:
---------
The ad_cache_memcache module allows for serving advertisements from multiple
web servers using a central memcache server.  It also improves performance
by caching advertisements and statistics in memory, only synchronizing
occasionally.

-------------
Installation:
-------------
The ad_cache_memcache module must be installed in the same directory that the
ad module was installed.  For example, if the ad module lives at
sites/default/modules/ad, then this modules should live at
sites/default/modules/ad_memcache.  (This is a temporary limitation that still
needs to be solved, caused because advertisements are served without
bootstrapping Drupal.)

NOTE:
This module is currently hard coded to talk to memcache at localhost:11211.

-------------
Requirements:
-------------
The ad_cache_memcache module requires that you have memcache PHP extensions
installed, and that memcached is running.  For more information about the
memcache extension, visit these pages:
  http://php.net/memcache
  http://pecl.php.net/package/memcache
